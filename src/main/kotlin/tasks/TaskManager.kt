package tasks

import java.io.IOException
import java.lang.IllegalArgumentException
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.collections.ArrayList
import kotlin.concurrent.read
import kotlin.concurrent.write

class AddProcessException : IOException("process not added")
class ProcessNotFoundException: IllegalArgumentException("process not found")

enum class ListOrder {
    ID, PRIORITY
}

interface TaskManager {
    fun add(priority: Priority)
    fun list(): List<Process>
    fun list(order: ListOrder): List<Process>
    fun kill(pid: Int)
    fun killGroup(priority: Priority)
    fun killAll()
}

abstract class AbstractTaskManager(protected var upperBound: Int, protected val processFactory: ProcessFactory): TaskManager {
    protected var pidCounter = 0
    protected val rwLock = ReentrantReadWriteLock()

    abstract var processList: MutableList<Process>

    init {
        if (upperBound == 0) {
            val prop = System.getProperty("tasks.upperBound") ?: ""
            upperBound = prop.toInt()
        }
    }

    override fun list(): List<Process> =
        rwLock.read { processList.toList() }

    override fun list(order: ListOrder): List<Process> =
        when (order) {
            ListOrder.ID -> rwLock.read {  processList.sortedBy { it.pid } }
            ListOrder.PRIORITY -> rwLock.read { processList.sortedBy { it.priority } }
        }

    override fun kill(pid: Int) {
        rwLock.write {
            val process =  processList.find { it.pid == pid }  ?: throw ProcessNotFoundException()

            process.kill()

            processList.remove(process)
        }
    }

    override fun killGroup(priority: Priority) {
        rwLock.write {
            val forRemoval = processList.filter { it.priority == priority }
            forRemoval.forEach { it.kill() }

            processList.removeAll(forRemoval)
        }
    }

    override fun killAll() {
        rwLock.write {
            processList.forEach { it.kill() }
            processList.clear()
        }
    }
}

class BoundedTaskManager(upperBound: Int, processFactory: ProcessFactory): AbstractTaskManager(upperBound, processFactory) {
    override var processList: MutableList<Process> = ArrayList()

    override fun add(priority: Priority) {
        rwLock.write {
            if (processList.size == upperBound) throw AddProcessException()

            val process = processFactory.createProcess(++pidCounter, priority)

            processList.add(process)
        }
    }
}

class FIFOTaskManager(upperBound: Int, processFactory: ProcessFactory): AbstractTaskManager(upperBound, processFactory) {
    override var processList: MutableList<Process> = LinkedList()

    override fun add(priority: Priority) {
        rwLock.write {
            if (processList.size == upperBound) {
                processList.removeFirst()
            }

            val process = processFactory.createProcess(++pidCounter, priority)

            processList.add(process)
        }
    }

}
class PriorityTaskManager(upperBound: Int, processFactory: ProcessFactory): AbstractTaskManager(upperBound, processFactory) {
    override var processList: MutableList<Process> = LinkedList()

    override fun add(priority: Priority) {
        rwLock.write {
            if (processList.size == upperBound) {
                val process = processList.find { it.priority < priority } ?: throw AddProcessException()
                processList.remove(process)
            }

            val process = processFactory.createProcess(++pidCounter, priority)

            processList.add(process)
        }
    }

}

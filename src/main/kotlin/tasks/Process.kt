package tasks

interface Process {
    val pid: Int
    val priority: Priority
    fun kill()
}

enum class Priority {
    LOW, MEDIUM, HIGH
}

abstract class ProcessFactory {
    abstract fun createProcess(pid: Int, priority: Priority): Process
}

class BasicProcess(override val pid: Int, override val priority: Priority) : Process {
    override fun kill() {
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BasicProcess

        if (pid != other.pid) return false
        if (priority != other.priority) return false

        return true
    }

    override fun hashCode(): Int {
        var result = pid
        result = 31 * result + priority.hashCode()
        return result
    }

    override fun toString(): String {
        return "BasicProcess(pid=$pid, priority=$priority)"
    }


}

class BasicProcessFactory: ProcessFactory() {
    override fun createProcess(pid: Int, priority: Priority) = BasicProcess(pid, priority)
}
package tasks

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.collections.listOf

class TaskManagerTest {
    @Test
    fun testBounded() {
        System.setProperty("tasks.upperBound", "2")
        val processFactory = tasks.BasicProcessFactory()
        val testManager = tasks.BoundedTaskManager(0, processFactory)

        testManager.add(Priority.LOW)
        testManager.add(Priority.MEDIUM)

        assertThrows<AddProcessException> {
            testManager.add(Priority.HIGH)
        }

        Assertions.assertEquals(listOf(BasicProcess(1, Priority.LOW), BasicProcess(2, Priority.MEDIUM)), testManager.list())

        testManager.kill(1)

        Assertions.assertEquals(listOf(BasicProcess(2, Priority.MEDIUM)), testManager.list())

        assertThrows<ProcessNotFoundException> {
            testManager.kill(3)
        }

        testManager.add(Priority.LOW)

        Assertions.assertEquals(listOf(BasicProcess(2, Priority.MEDIUM), BasicProcess(3, Priority.LOW)), testManager.list())

        Assertions.assertEquals(listOf(BasicProcess(3, Priority.LOW), BasicProcess(2, Priority.MEDIUM)), testManager.list(ListOrder.PRIORITY))

        Assertions.assertEquals(listOf(BasicProcess(2, Priority.MEDIUM), BasicProcess(3, Priority.LOW)), testManager.list(ListOrder.ID))

        testManager.killGroup(Priority.MEDIUM)

        Assertions.assertEquals(listOf(BasicProcess(3, Priority.LOW)), testManager.list())

        testManager.killAll()

        Assertions.assertTrue(testManager.list().isEmpty())
    }

    @Test
    fun testFIFO() {
        val processFactory = tasks.BasicProcessFactory()
        val testManager = tasks.FIFOTaskManager(2, processFactory)

        testManager.add(Priority.LOW)
        testManager.add(Priority.MEDIUM)
        testManager.add(Priority.HIGH)
        Assertions.assertEquals(listOf(BasicProcess(2, Priority.MEDIUM), BasicProcess(3, Priority.HIGH)), testManager.list(ListOrder.ID))
    }

    @Test
    fun testPriority() {
        val processFactory = tasks.BasicProcessFactory()
        val testManager = tasks.PriorityTaskManager(2, processFactory)

        testManager.add(Priority.LOW)
        testManager.add(Priority.MEDIUM)
        testManager.add(Priority.HIGH)
        Assertions.assertEquals(listOf(BasicProcess(2, Priority.MEDIUM), BasicProcess(3, Priority.HIGH)), testManager.list(ListOrder.ID))

        assertThrows<AddProcessException> {
            testManager.add(Priority.MEDIUM)
        }

        testManager.kill(3)

        testManager.add(Priority.LOW)
        Assertions.assertEquals(listOf(BasicProcess(2, Priority.MEDIUM), BasicProcess(4, Priority.LOW)), testManager.list(ListOrder.ID))
    }
}